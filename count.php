<!DOCTYPE html>
<html>
<head>
<style>
table {
    width: 100%;
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
    padding: 5px;
}

th {text-align: left;}
</style>
</head>
<body>




<br><br>
<hr>
<?php include('connection.php'); ?>
<?php

$NumberIndex = $_POST['numberIndex'];

$sql = "SELECT id, name, numberProduts, purchasePrice, productCategory FROM MyTable";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    echo "<table>
<tr>
<th>Index </th>
<th>Name </th>
<th>Number </th>
<th>Purchase Price</th>
<th>Product Category</th>
</tr>";
while($row = mysqli_fetch_array($result)) {
    echo "<tr>";
    echo "<td>" . $row['id'] . "</td>";
    echo "<td>" . $row['name'] . "</td>";
    echo "<td>" . $row['numberProduts'] . "</td>";
    $PurchPrice = $row['purchasePrice'];
    $NumberIndex += 1;
    $newPurchPrice = ($PurchPrice * $NumberIndex);
    echo "<td>" . $newPurchPrice . "</td>";
    echo "<td>" . $row['productCategory'] . "</td>";
    echo "</tr>";
}
echo "</table>";
} else {
    echo "0 results";
}
$conn->close();
?>


</body>
</html>

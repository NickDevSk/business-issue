<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="styles.css">
    <meta name="description" content="Business Issue">
    <meta name="keywords" content="Business Issue">
    <meta name="author" content="Nick Skotarenko">
    <meta charset="UTF-8">

    <title>Business Issue</title>
</head>
<body>

<br>

<div id="header">
<div class="upBlock">
<!-- Insert --> 
<form action="insert(name, number, purchase_price, product_category)" method="post">   
Name: <input type="text" name="name">
Number: <input type="number" step="0.01" name="number">
Purchase Price: <input type="number" step="0.01" name="purchase_price">
Product Category: <select name="product_category">
                    <option value="product">Product</option>
                    <option value="service">Service</option>
                  </select>
<input type="submit" value="Add">
</form>
</div>

<br><br>

<div class="upBlock">
<!-- Remove --> 
<form action="remove.php" method="post">   
ID: <input type="number" step="1" name="id">
<input type="submit" value="Remove">
</form>
</div>


<div class="upBlock">
<!-- Index -->
<form action="count.php" method="post">
Index: <input type="number" step="0.001" name="numberIndex">
<input type="submit" value="Count">
</form>

<br>

<!-- Update -->
<form action="index.php" method="post">
<input type="submit" value="Update">
</form>
</div>
</div>

<br><br>
<hr>
<?php include('connection.php'); ?>
<?php

$sql = "SELECT id, name, numberProduts, purchasePrice, productCategory FROM MyTable";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    echo "<table>
<tr>
<th>Index </th>
<th>Name </th>
<th>Number </th>
<th>Purchase Price</th>
<th>Product Category</th>
</tr>";
while($row = mysqli_fetch_array($result)) {
    echo "<tr>";
    echo "<td>" . $row['id'] . "</td>";
    echo "<td>" . $row['name'] . "</td>";
    echo "<td>" . $row['numberProduts'] . "</td>";
    echo "<td>" . $row['purchasePrice'] . "</td>";
    echo "<td>" . $row['productCategory'] . "</td>";
    echo "</tr>";
}
echo "</table>";
} else {
    echo "0 results";
}
$conn->close();
?>

</body>
</html>


